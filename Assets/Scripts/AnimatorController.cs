using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void SetBoolForMove(bool isMoving)
    {
        animator.SetBool("isMoving", isMoving);
    }

    public void SetBoolForDance(bool isDancing)
    {
        animator.SetBool("isDancing", isDancing);
    }
}
