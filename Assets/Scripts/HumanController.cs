using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HumanController : MonoBehaviour
{
    public float wanderRadius;

    private NavMeshAgent agent;
    private float timer;
    private bool isTimerStart;

    private AnimatorController animatorController;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animatorController = GetComponent<AnimatorController>();
        isTimerStart = true;
        timer = 0;
    }

    void Update()
    {

        if (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance)
        {
            if (!isTimerStart)
            {
                isTimerStart = true;
                timer = Random.Range(1.0f, 8.0f);

                animatorController.SetBoolForMove(false);

                if (NavMesh.SamplePosition(transform.position, out NavMeshHit hit, 0.1f, 32))
                    animatorController.SetBoolForDance(true);
                else
                    animatorController.SetBoolForDance(false);
            }

            else
            {
                timer -= Time.deltaTime;
                if (timer <= 0)
                {

                    int randomLayer = Random.Range(3, 6);
                    int randomLayerMask = 1 << randomLayer;

                    Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, randomLayerMask);
                    agent.SetDestination(newPos);
                    isTimerStart = false;
                    animatorController.SetBoolForMove(true);
                }
            }
        }
    }

    private Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

}
