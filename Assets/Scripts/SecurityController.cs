using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SecurityController : MonoBehaviour
{
    private NavMeshAgent agent;
    private float timer;
    private bool isTimerStart;

    private AnimatorController animatorController;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animatorController = GetComponent<AnimatorController>();
        isTimerStart = true;
        timer = 0;
    }

    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance)
        {
            if (!isTimerStart)
            {
                isTimerStart = true;
                timer = Random.Range(1.0f, 8.0f);
                animatorController.SetBoolForMove(false);
            }
            else
            {
                timer -= Time.deltaTime;
                if (timer <= 0)
                {
                    GameObject[] array = GameObject.FindGameObjectsWithTag("Human");
                    int randIndex = Random.Range(0, array.Length);
                    NavMeshHit hit;
                    if (NavMesh.SamplePosition(array[randIndex].transform.position, out hit, 0.1f, agent.areaMask))
                    {
                        agent.isStopped = false;
                        agent.SetDestination(array[randIndex].transform.position);
                        isTimerStart = false;
                        animatorController.SetBoolForMove(true);
                    }
                    else
                    {
                        agent.isStopped = true;
                        animatorController.SetBoolForMove(false);
                    }
                }
            }
        }
    }
}
